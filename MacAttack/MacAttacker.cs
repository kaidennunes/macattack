﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace MacAttack
{
	public class MacAttacker
	{
		private static readonly int SHA_1_PADDING_BIT_SIZE = 512;
		private static readonly int SHA_1_FIXED_PADDING_BIT_SIZE = 448;

		private MacHolder originalMacHolder = new MacHolder();

		public MacAttacker(string originalMessage, string originalDigest, int originalKeyLength)
		{
			string hexStringMessage = Utils.StringToHexString(originalMessage);
			originalMacHolder.Message = Utils.HexStringToByteArray(hexStringMessage);

			string hexStringDigest = Utils.StringToHexString(originalDigest);
			originalMacHolder.Digest = Utils.HexStringToByteArray(hexStringDigest);

			originalMacHolder.KeyLength = originalKeyLength;
		}

		public MacAttacker(string originalMessage, byte[] originalDigest, int originalKeyLength)
		{
			string hexStringMessage = Utils.StringToHexString(originalMessage);
			originalMacHolder.Message = Utils.HexStringToByteArray(hexStringMessage);

			originalMacHolder.Message = Encoding.Default.GetBytes(originalMessage);
			originalMacHolder.Digest = originalDigest;

			originalMacHolder.KeyLength = originalKeyLength;
		}

		public MacAttacker(byte[] originalMessage, byte[] originalDigest, int originalKeyLength)
		{
			originalMacHolder.Message = originalMessage;
			originalMacHolder.Digest = originalDigest;
			originalMacHolder.KeyLength = originalKeyLength;
		}

		public MacHolder GetExtendedMessage(string extendedMessage)
		{
			string hexString = Utils.StringToHexString(extendedMessage);
			return GetExtendedMessage(Utils.HexStringToByteArray(hexString));
		}

		public MacHolder GetExtendedMessage(byte[] extendedMessage)
		{
			byte[] paddedMessage = PadByteArray(originalMacHolder.Message, SHA_1_PADDING_BIT_SIZE, SHA_1_FIXED_PADDING_BIT_SIZE);
			byte[] paddedExtendedMessage = Utils.Combine(paddedMessage, extendedMessage);

			byte[] newDigest = CalculateSha1HashWithState(extendedMessage, originalMacHolder.Digest, (uint)(paddedMessage.Length + originalMacHolder.KeyLength) * 8);

			MacHolder macHolder = new MacHolder
			{
				Message = paddedExtendedMessage,
				Digest = newDigest
			};
			return macHolder;
		}

		private byte[] PadByteArray(byte[] data, int paddingBitLength, int fixedBitPaddingLength)
		{
			int currentByteMessageLength = data.Length + originalMacHolder.KeyLength;

			byte[] pad;
			int padLen;
			long bitCount;

			/* Compute padding: 80 00 00 ... 00 00 <bit count>
			          */

			padLen = 64 - (currentByteMessageLength & 0x3f);
			if (padLen <= 8)
				padLen += 64;

			pad = new byte[padLen];
			pad[0] = 0x80;

			//  Convert count to bit count
			bitCount = currentByteMessageLength * 8;

			pad[padLen - 8] = (byte)((bitCount >> 56) & 0xff);
			pad[padLen - 7] = (byte)((bitCount >> 48) & 0xff);
			pad[padLen - 6] = (byte)((bitCount >> 40) & 0xff);
			pad[padLen - 5] = (byte)((bitCount >> 32) & 0xff);
			pad[padLen - 4] = (byte)((bitCount >> 24) & 0xff);
			pad[padLen - 3] = (byte)((bitCount >> 16) & 0xff);
			pad[padLen - 2] = (byte)((bitCount >> 8) & 0xff);
			pad[padLen - 1] = (byte)((bitCount >> 0) & 0xff);

			return Utils.Combine(data, pad);
		}

		private byte[] CalculateSha1HashWithState(byte[] data, byte[] state, uint paddedSize)
		{
			byte[] hash = null;
			using (Stream stream = new MemoryStream(data))
			{
				var hashedData = SHA1Stateful.HashFile(stream, state, paddedSize);
				hash = hashedData.ToArray();
			}
			return hash;
		}
	}
}
