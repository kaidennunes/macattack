﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace MacAttack
{
	public static class Utils
	{
		public static byte[] ToByteArray(object anyValue, int length)
		{
			if (length > 0)
			{
				int rawsize = Marshal.SizeOf(anyValue);
				IntPtr buffer = Marshal.AllocHGlobal(rawsize);
				Marshal.StructureToPtr(anyValue, buffer, false);
				byte[] rawdatas = new byte[rawsize * length];
				Marshal.Copy(buffer, rawdatas, (rawsize * (length - 1)), rawsize);
				Marshal.FreeHGlobal(buffer);
				return rawdatas;
			}
			return new byte[0];
		}

		public static string ByteArrayToHexString(byte[] data)
		{
			StringBuilder hexString = new StringBuilder(data.Length * 2);
			foreach (byte byteData in data)
			{
				hexString.AppendFormat("{0:x2}", byteData);
			}
			return hexString.ToString();
		}

		public static byte[] HexStringToByteArray(string hex)
		{
			return Enumerable.Range(0, hex.Length)
							 .Where(x => x % 2 == 0)
							 .Select(x => Convert.ToByte(hex.Substring(x, 2), 16))
							 .ToArray();
		}

		public static string StringToHexString(string data)
		{
			StringBuilder hexData = new StringBuilder();
			char[] values = data.ToCharArray();
			foreach (char letter in values)
			{
				// Get the integral value of the character.
				int value = Convert.ToInt32(letter);
				hexData.Append(value.ToString("X"));
			}
			return hexData.ToString();
		}

		public static byte[] Combine(params byte[][] arrays)
		{
			byte[] rv = new byte[arrays.Sum(a => a.Length)];
			int offset = 0;
			foreach (byte[] array in arrays)
			{
				Buffer.BlockCopy(array, 0, rv, offset, array.Length);
				offset += array.Length;
			}
			return rv;
		}
	}
}
