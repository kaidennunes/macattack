﻿/*
 * Copyright (c) 2010 Yuri K. Schlesner
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;

namespace MacAttack
{
	public class SHA1Stateful
	{
		private ulong BitsAlreadyRead = 0;
		private static readonly uint[] K = new uint[80] {
			0x5a827999,0x5a827999,0x5a827999,0x5a827999,0x5a827999,0x5a827999,0x5a827999,0x5a827999,0x5a827999,0x5a827999,
			0x5a827999,0x5a827999,0x5a827999,0x5a827999,0x5a827999,0x5a827999,0x5a827999,0x5a827999,0x5a827999,0x5a827999,
			0x6ed9eba1,0x6ed9eba1,0x6ed9eba1,0x6ed9eba1,0x6ed9eba1,0x6ed9eba1,0x6ed9eba1,0x6ed9eba1,0x6ed9eba1,0x6ed9eba1,
			0x6ed9eba1,0x6ed9eba1,0x6ed9eba1,0x6ed9eba1,0x6ed9eba1,0x6ed9eba1,0x6ed9eba1,0x6ed9eba1,0x6ed9eba1,0x6ed9eba1,
			0x8f1bbcdc,0x8f1bbcdc,0x8f1bbcdc,0x8f1bbcdc,0x8f1bbcdc,0x8f1bbcdc,0x8f1bbcdc,0x8f1bbcdc,0x8f1bbcdc,0x8f1bbcdc,
			0x8f1bbcdc,0x8f1bbcdc,0x8f1bbcdc,0x8f1bbcdc,0x8f1bbcdc,0x8f1bbcdc,0x8f1bbcdc,0x8f1bbcdc,0x8f1bbcdc,0x8f1bbcdc,
			0xca62c1d6,0xca62c1d6,0xca62c1d6,0xca62c1d6,0xca62c1d6,0xca62c1d6,0xca62c1d6,0xca62c1d6,0xca62c1d6,0xca62c1d6,
			0xca62c1d6,0xca62c1d6,0xca62c1d6,0xca62c1d6,0xca62c1d6,0xca62c1d6,0xca62c1d6,0xca62c1d6,0xca62c1d6,0xca62c1d6
		};

		private static uint F(int t, uint x, uint y, uint z)
		{
			if (t <= 19)
			{
				return Ch(x, y, z);
			}
			else if (t <= 39)
			{
				return Parity(x, y, z);
			}
			else if (t <= 59)
			{
				return Maj(x, y, z);
			}
			else if (t <= 79)
			{
				return Parity(x, y, z);
			}
			return 0;
		}

		private static uint Parity(uint x, uint y, uint z)
		{
			return x ^ y ^ z;
		}

		private static uint ROTL(uint x, byte n)
		{
			return (x << n) | (x >> (32 - n));
		}

		private static uint Ch(uint x, uint y, uint z)
		{
			return (x & y) ^ ((~x) & z);
		}

		private static uint Maj(uint x, uint y, uint z)
		{
			return (x & y) ^ (x & z) ^ (y & z);
		}

		private uint[] State = new uint[5] {
			0x67452301, 0xefcdab89, 0x98badcfe, 0x10325476, 0xc3d2e1f0
		};

		private byte[] pending_block = new byte[64];
		private uint pending_block_off = 0;
		private uint[] uint_buffer = new uint[16];

		private ulong bits_processed = 0;

		private bool closed = false;

		private void ProcessBlock(uint[] M)
		{
			// 1. Prepare the message schedule (W[t]):
			uint[] W = new uint[80];
			for (int t = 0; t < 16; ++t)
			{
				W[t] = M[t];
			}

			for (int t = 16; t < 80; ++t)
			{
				W[t] = ROTL(W[t - 3] ^ W[t - 8] ^ W[t - 14] ^ W[t - 16], 1);
			}

			// 2. Initialize the eight working variables with the (i-1)-st hash value:
			uint a = State[0],
				   b = State[1],
				   c = State[2],
				   d = State[3],
				   e = State[4];

			// 3. For t=0 to 79:
			for (int t = 0; t < 80; ++t)
			{
				uint T = ROTL(a, 5) + F(t, b, c, d) + e + K[t] + W[t];
				e = d;
				d = c;
				c = ROTL(b, 30);
				b = a;
				a = T;
			}

			// 4. Compute the intermediate hash value H:
			State[0] = a + State[0];
			State[1] = b + State[1];
			State[2] = c + State[2];
			State[3] = d + State[3];
			State[4] = e + State[4];
		}

		public void AddData(byte[] data, uint offset, uint len)
		{
			if (closed)
				throw new InvalidOperationException("Adding data to a closed hasher.");

			if (len == 0)
				return;

			bits_processed += len * 8;

			while (len > 0)
			{
				uint amount_to_copy;

				if (len < 64)
				{
					if (pending_block_off + len > 64)
						amount_to_copy = 64 - pending_block_off;
					else
						amount_to_copy = len;
				}
				else
				{
					amount_to_copy = 64 - pending_block_off;
				}

				Array.Copy(data, offset, pending_block, pending_block_off, amount_to_copy);
				len -= amount_to_copy;
				offset += amount_to_copy;
				pending_block_off += amount_to_copy;

				if (pending_block_off == 64)
				{
					toUintArray(pending_block, uint_buffer);
					ProcessBlock(uint_buffer);
					pending_block_off = 0;
				}
			}
		}

		public ReadOnlyCollection<byte> GetHash()
		{
			return toByteArray(GetHashUInt32());
		}

		public ReadOnlyCollection<uint> GetHashUInt32()
		{
			if (!closed)
			{
				UInt64 size_temp = bits_processed + BitsAlreadyRead;

				AddData(new byte[1] { 0x80 }, 0, 1);

				uint available_space = 64 - pending_block_off;

				if (available_space < 8)
					available_space += 64;

				// 0-initialized
				byte[] padding = new byte[available_space];
				// Insert lenght uint64
				for (uint i = 1; i <= 8; ++i)
				{
					padding[padding.Length - i] = (byte)size_temp;
					size_temp >>= 8;
				}

				AddData(padding, 0u, (uint)padding.Length);

				Debug.Assert(pending_block_off == 0);

				closed = true;
			}

			return Array.AsReadOnly(State);
		}

		private static void toUintArray(byte[] src, uint[] dest)
		{
			for (uint i = 0, j = 0; i < dest.Length; ++i, j += 4)
			{
				dest[i] = ((uint)src[j + 0] << 24) | ((uint)src[j + 1] << 16) | ((uint)src[j + 2] << 8) | ((uint)src[j + 3]);
			}
		}

		private static ReadOnlyCollection<byte> toByteArray(ReadOnlyCollection<uint> src)
		{
			byte[] dest = new byte[src.Count * 4];
			int pos = 0;

			for (int i = 0; i < src.Count; ++i)
			{
				dest[pos++] = (byte)(src[i] >> 24);
				dest[pos++] = (byte)(src[i] >> 16);
				dest[pos++] = (byte)(src[i] >> 8);
				dest[pos++] = (byte)(src[i]);
			}

			return Array.AsReadOnly(dest);
		}


		public static ReadOnlyCollection<byte> HashFile(Stream fs, byte[] state, uint originalSize)
		{
			SHA1Stateful sha = new SHA1Stateful();

			uint[] decoded = new uint[5];
			Buffer.BlockCopy(state, 0, decoded, 0, state.Length);

			decoded[0] = SwapBytes(decoded[0]);
			decoded[1] = SwapBytes(decoded[1]);
			decoded[2] = SwapBytes(decoded[2]);
			decoded[3] = SwapBytes(decoded[3]);
			decoded[4] = SwapBytes(decoded[4]);

			sha.State = decoded;
			sha.BitsAlreadyRead = originalSize;

			byte[] buf = new byte[8196];

			uint bytes_read;
			do
			{
				bytes_read = (uint)fs.Read(buf, 0, buf.Length);
				if (bytes_read == 0)
					break;

				sha.AddData(buf, 0, bytes_read);
			}
			while (bytes_read == 8196);

			return sha.GetHash();
		}

		private static uint SwapBytes(uint x)
		{
			// swap adjacent 16-bit blocks
			x = (x >> 16) | (x << 16);
			// swap adjacent 8-bit blocks
			return ((x & 0xFF00FF00) >> 8) | ((x & 0x00FF00FF) << 8);
		}
	}
}