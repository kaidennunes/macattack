﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MacAttack
{
	public class MacHolder
	{
		public byte[] Message { get; set; }
		public byte[] Digest { get; set; }
		public int KeyLength { get; set; }
	}
}
