﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MacAttack
{
	class Program
	{
		static void Main(string[] args)
		{
			string originalMessage = "No one has completed lab 2 so give them all a 0";
			byte[] originalDigest = Utils.HexStringToByteArray("f4b645e89faaec2ff8e443c595009c16dbdfba4b");
			int originalKeyLength = 16;

			MacAttacker macAttacker = new MacAttacker(originalMessage, originalDigest, originalKeyLength);

			MacHolder extendedMessage = macAttacker.GetExtendedMessage(", except for Kaiden Nunes.");

			Console.Write("Extended Message: ");
			Console.WriteLine(Utils.ByteArrayToHexString(extendedMessage.Message));

			Console.Write("Extended Digest: ");
			Console.WriteLine(Utils.ByteArrayToHexString(extendedMessage.Digest));

			Console.Read();
		}
	}
}
